package PomConstant;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

public class Scenario1 {

	public static WebDriver driver = null;
	public static PomConstants page = new PomConstants();
  @Test
  public void f() throws InterruptedException 
  {
		@SuppressWarnings("static-access")
		String randomOrigin = page.getRandomOrigin();
		WebElement origin = driver.findElement(By.id(page.ORIGIN_ID));
		origin.sendKeys(randomOrigin);
		
		@SuppressWarnings("static-access")
		String randomDestination = page.getRandomDestination();
		WebElement destination = driver.findElement(By.id(page.DESTINATION_ID));
		destination.sendKeys(randomDestination);
		
		//open passanger's drop down 
		driver.findElement(By.id(page.PASSANGER_INPUT_ID_CLICK)).click();
		Thread.sleep(1000);
		//increment passaneger's drop down
		driver.findElement(By.xpath(page.PASSANGER_INCREMENT_XPATH_CLICK)).click();
		Thread.sleep(2000);
		//close passanger's drop down 
		driver.findElement(By.id(page.PASSANGER_INPUT_ID_CLICK)).click();
		Thread.sleep(4000);
		
		driver.findElement(By.id(page.SEARCH_BUTTON_ID)).click();
		driver.navigate().refresh(); 
		
		//Wait for 20 Sec
		Thread.sleep(20000);
		driver.findElement(By.xpath(page.SORT_ARROW_BUTTON_XPATH)).click();
		System.out.println(page.SORT_SUCCESS_MSG);
		Thread.sleep(3000);
		
		String highestValue = driver.findElement(By.className(page.FIRST_ITEM_PRICE_CLASS)).getText();;
		System.out.println("Highest Price is :" + highestValue);
  }
  @BeforeMethod
  public void beforeMethod() 
  {
	  System.setProperty(PomConstants.DRIVER_TYPE_CHROME, PomConstants.DRIVER_PATH);
	  driver = new ChromeDriver();
	  driver.navigate().to(PomConstants.URL);
	  driver.manage().window().maximize();
  }

  @AfterMethod
  public void afterMethod() 
  {
	  
  }

}
