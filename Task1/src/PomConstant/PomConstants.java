package PomConstant;

import java.util.Random;

public class PomConstants {
	
	public String SORT_ARROW_BUTTON_XPATH1 = "//*[@id='search-result__container']/div[1]/div/flights-summary-wrapper/div/div[2]/div[1]/div[5]/span";
	
	public static String DRIVER_PATH = "D:\\chromedriver.exe";
	public static String DRIVER_TYPE_CHROME = "webdriver.chrome.driver";
	public static String URL = "https://www.tajawal.ae/en";
	public String ORIGIN_ID = "flights-search-origin-1";
	public String DESTINATION_ID = "flights-search-destination-1";
	public String PASSANGER_INPUT_ID_CLICK = "flights-search-open-pax-btn";
	public String PASSANGER_INCREMENT_XPATH_CLICK = "//*[@id='rbFlightTabContent']/div/div/section/div/div[1]/form/div[3]/div[1]/div[3]/div[2]/div[1]/div[2]/a[2]";
	public String SEARCH_BUTTON_ID = "flights-search-cta";
	public String SORT_ARROW_BUTTON_XPATH = "//*[@id='search-result__container']/div[1]/div/flights-summary-wrapper/div/div[2]/div[1]/div[5]/span";
	public String FIRST_ITEM_PRICE_CLASS = "search-result-card__price";
	public String SORT_SUCCESS_MSG = "Sort is applied succsfully:";
	
	static Random random = new Random();

	public static String getRandomOrigin()
	{
		String[] originsArray = { "KHI Karachi, Pakistan", "LHE Lahore, Pakistan", "ISB Islamabad, Pakistan", "MUX Multan, Pakistan"};
		return originsArray[random.nextInt(originsArray.length)];
	}
	
	public static String getRandomDestination()
	{
		String[] destinationsArray = {"SHJ Sharjah, United Arab Emirates", "DXB Dubai, United Arab Emirates", "LHR London, United Kingdom - London Heathrow Airport", "LAS Las Vegas, United States - McCarran International Airport", "IND Indianapolis, United States"};
		return destinationsArray[random.nextInt(destinationsArray.length)];
	}

}
