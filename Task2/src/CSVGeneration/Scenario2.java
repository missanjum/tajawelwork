package CSVGeneration;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.csvreader.CsvWriter;

public class Scenario2 {
	public static WebDriver driver=null;
  @Test
  public void f() throws InterruptedException 
  {
	  String csvOutputFile = "writetest.csv";
	  Random random = new Random();
		String[] originsArray = { "KHI Karachi, Pakistan", "LHE Lahore, Pakistan", "ISB Islamabad, Pakistan", "MUX Multan, Pakistan"};
		WebElement origin = driver.findElement(By.id("flights-search-origin-1"));
		origin.sendKeys(originsArray[random.nextInt(originsArray.length)]);
		
		String[] destinationsArray = {"SHJ Sharjah, United Arab Emirates", "DXB Dubai, United Arab Emirates", "LHR London, United Kingdom - London Heathrow Airport", "LAS Las Vegas, United States - McCarran International Airport", "IND Indianapolis, United States"};
		
		WebElement destination = driver.findElement(By.id("flights-search-destination-1"));
		destination.sendKeys(destinationsArray[random.nextInt(destinationsArray.length)]);
		
		driver.findElement(By.id("flights-search-open-pax-btn")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id='rbFlightTabContent']/div/div/section/div/div[1]/form/div[3]/div[1]/div[3]/div[2]/div[1]/div[2]/a[2]")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("flights-search-open-pax-btn")).click();
		Thread.sleep(4000);
		driver.findElement(By.id("flights-search-cta")).click();
		 
		//Wait for 20 Sec
		Thread.sleep(20000);
		WebElement priceSorting=driver.findElement(By.xpath("//*[@id=\"search-result__container\"]/div[1]/div/flights-summary-wrapper/div/div[2]/div[1]/div[5]/span/span"));
		priceSorting.click();
		
		Thread.sleep(3000);
		
		List<WebElement> categories = driver.findElements(By.xpath("//div[@class='search-result-card__price']"));
		
		
		
		//check if file exist
        boolean isFileExist = new File(csvOutputFile).exists();
            try {
                //create a FileWriter constructor to open a file in appending mode
                CsvWriter testcases = new csvwriter(new FileWriter(csvOutputFile, true), ',');
                //write header column if the file did not already exist
                if(!isFileExist)
                {
                    testcases.write("Price Decending");
                    //end the record
                    testcases.endRecord();
                }
             //add record to the file
                for(WebElement w:categories)
                for(int i=0;i<=categories.size();i++){
                testcases.write(w.getText());
                testcases.endRecord();
                }
                
                //end the record
                
                //close the file
                testcases.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
		
		
		//write header column if the file did not already exist
		
		
		String highestValue=driver.findElement(By.className("search-result-card__price")).getText();
		System.out.println("First Price is :"+highestValue);
  }
  @BeforeMethod
  public void beforeMethod() 
  {
	  System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		// Create a new instance of the Chrome driver
		 driver = new ChromeDriver();
		driver.navigate().to("https://www.tajawal.ae/en");
		driver.manage().window().maximize();
  }

  @AfterMethod
  public void afterMethod() 
  {
	  
  }

}
