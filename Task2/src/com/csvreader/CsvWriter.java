package com.csvreader;

public interface CsvWriter {

	void write(String string);

	void endRecord();

	void close();

}
