Pre-Requisite for Running the source code and test scripts

1- Browsers:
Chrome 
Firefox 

with both gecko and chrome drivers

2- Selenium Webdriver 3 (Jars and Libraries)
TestNG Framework
Maven m2e integartion with Eclipse 
Extent Report for html reporting 
POM (Page Object model) structure 
Dependencies 
========================================================================

A - Tajawel Search Flights and access the Result page Automation 

Scenario 1 & Scenario 2 Code  Guide for Understanding:
========================================================================
Step1:

Create the First of All project in Eclipse 

Step 2:
Then Add the Selenium 3 all jar files and import all library to that creating project.

Step 3: 
Then create a package and make a class inside package
Set the Structure of Directory in the way like :

AutomationFramwork > Pages > Drivers>Dependencies>Reports

Step 4:

After that Open the Chrome browser instance ... Here is the optional either you can use FF or chrome
I run with both and selected Chrome for  the time being.

Step 5:

I used POM (Page Object model) structure ... I kept all the Web Elements , objects and Locators in "HomePageObjects.class" file
Used that file WebElements  in the Pages that we are going to Automate.
No hardcoded in the main pages all the Objects and classes are calling from Central "HomePageObjects.class" file.

Open a URL of Tajawel Airlines site

Step 6:

Maximize the window by using method

Step 7:

Called the Following fields from Home Page:

- Selected Random origins
- Selecting the Random Destination 
- Selecting the Date from Start to End
- Selecting the Passengers (Adults at least 2) 
- Click on Search CTA

Step 8.
Search Result page accessible and applied the sorting

Find the First price of the fligh on the search result page and fetch in the code output screen.

Step 9:

On listing page sort results by lower price (Ascending)

Step 10:
Get the First Flight price and Fetch the Price listing in CSV file.

Step 11:

The Output folder generate the CSV and records in it.


========================================


